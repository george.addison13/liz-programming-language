extern void printbool(int);
extern void printfloat(float);
extern void printint(int);
extern float get_float( void);
extern int get_int( void);
extern int addints(int a,...);
extern int multiplyints(int a,...); 
extern void dupintarray(int *a,int *b,int  l);
extern void dupfloatarray(float *a,float *b,int  l);
extern int search_index(float *a,int l , float key );
extern void sortintarray(int a[],int l);
extern void sortfloatarray(float a[],int l);
extern int  factorial(int a);
extern int permutation(int n,int r);
extern int combination(int n,int r) ;
extern float ap_nterm(float a,float d,float n); 
extern float ap_sumnterms(float a,float d,float n);
extern float gp_nterm(float a,float r, float n );
extern float gp_suminfinity(float a,float r);
extern float sine(float x);
extern float cosine(float x);
extern float tangent(float x);
extern float n_first(float u,float a,float t);
extern float n_second(float u,float a,float t);
extern float n_third(float u,float a,float s);
extern float C2F(float c);
extern float F2C(float f);
extern float slope(int x1,int x2,int y1,int y2);
extern float y_intercept(float m,float x,float c);
extern float work(float f,float d,float a);
extern float PE(float m,float g,float h);
extern float KE(float m,float v);
extern void display(char *w);
extern void printchar(char t);
extern float ShowMeanTable(float * a ,int len );
extern float ShowStdTable(float * a,int len );


#define PIE	(3.14159265358979323846)

//Avagadro Constant

#define AC  (6.022 * pow( 10 ,23) )

//Faradays Constant

#define FC  ( 9.649 * pow(10,4))


//acceleration due to gravity

#define AG ( 9.8) 

//speed of light 
 
#define SL  (2.9979 * pow(10,8))


/** General Routines **/

#define mini(a,b)   ((a < b) ?  a : b)

#define maxi(a,b)   ((a > b) ?   a : b) 

#define abs_val(a)  ((a < 0 ) ? -(a) : (a)) 

#define  get_char()     getchar()
	

/**Areas, Circumference  and Volumes */
	
	
#define  a_circle(r)   (3.14159265358979323846 * pow(r,2))

#define  c_circle(r)  	(2 * 3.14159265358979323846 * r )

#define  a_rectangle(l,b) ( l * b)

#define  p_rectangle(l ,b) (2*l +  2* b)
	
#define  a_square(a)  pow(a,2)

#define  p_square(a)  (4 * a)

#define v_cylinder(r,h) (3.14159265358979323846 * pow(r,2) * h )	
	
	
 
 
